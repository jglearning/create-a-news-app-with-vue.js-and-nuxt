import axios from '~/plugins/axios'

export const state = () => ({
  ids: [],
  itesm: []
})

export const mutations = {
  setIds (state, ids) {
    state.ids = ids
  },
  setItems (state, items) {
    state.items = items
  }
}

export const actions = {
  async LOAD_ITEMS ({
    commit
  }, dataUrl) {
    const response = await axios.get(dataUrl)
    const ids = response.data
    const tenIds = ids.slice(0, 10)
    const itemPromises = tenIds.map(id => axios.get(`item/${id}.json`))
    const itemResponses = await Promise.all(itemPromises)
    const items = itemResponses.map(res => res.data)
    const realItems = items.map(item => item || {title: 'Failed to load', id: 0})
    // commit("setIds", ids);
    commit('setItems', realItems)
  }
}

/*
asyncData(){
      return axios.get("ids")
        .then(res=>({
          ids: res.data
        }))
    }
    */
